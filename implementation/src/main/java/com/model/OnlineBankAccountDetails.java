package com.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
public class OnlineBankAccountDetails {

    @Id
    @NotNull
    private String id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String iban;

    @NotNull
    private String accountNumber;

    private int wrongPassCount;

    private LocalDateTime blockTime;

    public boolean isAccountNonLocked() {
        return this.blockTime == null
                || this.blockTime.plusHours(24).isBefore(LocalDateTime.now());
    }

}
