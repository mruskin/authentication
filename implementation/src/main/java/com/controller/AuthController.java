package com.controller;

import com.auth.api.v1.model.CreateAccountResponse;
import com.service.AuthService;
import com.service.OnlineBankAccountService;
import com.auth.api.v1.AuthApi;
import com.auth.api.v1.model.AuthRequest;
import com.auth.api.v1.model.AuthResponse;
import com.auth.api.v1.model.CreateAccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController implements AuthApi {

    @Autowired
    private AuthService authService;

    @Autowired
    private OnlineBankAccountService onlineBankAccountService;

    @Override
    public ResponseEntity<CreateAccountResponse> createOnlineBankingAccount(@Valid CreateAccountRequest createAccountRequest) {
        CreateAccountResponse response = onlineBankAccountService.createAccount(createAccountRequest);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<AuthResponse> createToken(@Valid AuthRequest authRequest) {
        AuthResponse response = authService.checkAndGenerateToken(authRequest.getUsername(), authRequest.getPassword());
        return ResponseEntity.ok(response);
    }

}
