package com.service;

import com.auth.api.v1.model.AuthResponse;
import com.exception.AuthException;
import com.model.OnlineBankAccountDetails;
import com.repository.OnlineBankAccountDetailsRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AuthService {

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    @Value("${auth.wrongPassCount}")
    private int wrongPassCount;

    @Autowired
    private OnlineBankAccountDetailsRepository onlineBankAccountDetailsRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    public AuthResponse checkAndGenerateToken(String username, String password) {
        authenticate(username, password);
        final String token = jwtTokenUtil.generateToken(username);
        AuthResponse authResponse = new AuthResponse();
        authResponse.setAccessToken(token);
        return authResponse;
    }

    private void authenticate(String username, String password)  {
        final OnlineBankAccountDetails accountDetails = onlineBankAccountDetailsRepository.findByUsername(username);
        if (accountDetails != null) {
            if (!accountDetails.isAccountNonLocked()) {
                throw new AuthException("This account is locked: " + accountDetails.getUsername());
            } else if (!(DigestUtils.sha256Hex(password)).equals(accountDetails.getPassword())) {
                log.info("Wrong password for user: " + username);
                wrongPassIncrease(accountDetails);
                throw new AuthException("Wrong username or password: " + accountDetails.getUsername());
            }
        } else {
            log.info("Wrong username: " + username);
            throw new AuthException( "Wrong username or password");
        }
    }

    public void wrongPassIncrease(final OnlineBankAccountDetails accountDetails) {
        if (accountDetails == null) {
            return;
        }
        if (accountDetails.getWrongPassCount() + 1 >= wrongPassCount) {
            accountDetails.setBlockTime(LocalDateTime.now());
            accountDetails.setWrongPassCount(0);
        } else {
            accountDetails.setWrongPassCount(accountDetails.getWrongPassCount() + 1);
        }
        onlineBankAccountDetailsRepository.save(accountDetails);
    }

}
