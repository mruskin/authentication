package com.service;

import com.auth.api.v1.model.CreateAccountRequest;
import com.auth.api.v1.model.CreateAccountResponse;

public interface OnlineBankAccountService {

    CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest);

}
