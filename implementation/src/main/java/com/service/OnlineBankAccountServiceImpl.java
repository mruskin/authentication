package com.service;

import com.auth.api.v1.model.CreateAccountRequest;
import com.auth.api.v1.model.CreateAccountResponse;
import com.exception.AccountNotFoundException;
import com.model.OnlineBankAccountDetails;
import com.repository.OnlineBankAccountDetailsRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OnlineBankAccountServiceImpl implements OnlineBankAccountService {

    private static final Logger log = LoggerFactory.getLogger(OnlineBankAccountServiceImpl.class);

    @Value("${endpoint.account-api}")
    private String accountApiEndpoint;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private OnlineBankAccountDetailsRepository onlineBankAccountDetailsRepository;

    @Override
    public CreateAccountResponse createAccount(final CreateAccountRequest createAccountRequest) {
        final CreateAccountResponse account = findAccount(createAccountRequest.getAccountNumber());
        log.info("Account found: " + account.getIban());
        OnlineBankAccountDetails onlineBankAccountDetails = new OnlineBankAccountDetails();
        onlineBankAccountDetails.setId(account.getOwnerId());
        onlineBankAccountDetails.setUsername(createAccountRequest.getUsername());
        onlineBankAccountDetails.setPassword(DigestUtils.sha256Hex(createAccountRequest.getPassword()));
        onlineBankAccountDetails.setIban(account.getIban());
        onlineBankAccountDetails.setAccountNumber(createAccountRequest.getAccountNumber());
        onlineBankAccountDetailsRepository.save(onlineBankAccountDetails);
        log.info("Account created: " + account.getIban() + ", " + createAccountRequest.getUsername());
        return account;
    }

    private CreateAccountResponse findAccount(final String accountNumber) {
        try {
            return restTemplate
                    .getForEntity(accountApiEndpoint + accountNumber, CreateAccountResponse.class)
                    .getBody();
        } catch (Exception ex) {
            log.info("Account search issue: " + ex.getMessage());
            throw new AccountNotFoundException("Account not found in Accounts API - " + accountNumber);
        }
    }

}
