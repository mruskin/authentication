package com.repository;

import com.model.OnlineBankAccountDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OnlineBankAccountDetailsRepository extends JpaRepository<OnlineBankAccountDetails, String> {

    OnlineBankAccountDetails findByUsername(String username);
    OnlineBankAccountDetails findByUsernameAndPassword(String username, String password);
}
