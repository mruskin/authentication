package com.controller;

import com.auth.api.v1.model.AuthRequest;
import com.auth.api.v1.model.AuthResponse;
import com.auth.api.v1.model.CreateAccountRequest;
import com.auth.api.v1.model.CreateAccountResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.repository.OnlineBankAccountDetailsRepository;
import com.service.AuthService;
import com.service.JwtTokenUtil;
import com.service.OnlineBankAccountService;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@EnableSpringDataWebSupport
@WebMvcTest(value = AuthController.class, secure = false)
public class AuthControllerTest {

    @Autowired
    protected MockMvc mvc;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @MockBean
    protected AuthService service;

    @MockBean
    protected OnlineBankAccountService onlineBankAccountService;

    @MockBean
    protected OnlineBankAccountDetailsRepository repository;

    @MockBean
    protected JwtTokenUtil jwtTokenUtil;

    @Autowired
    protected ObjectMapper objectMapper;

    @Test
    public void testCreateOnlineBankingAccountSuccess() throws Exception {
        // given
        final CreateAccountResponse account = getAccountResponse();

        // when
        Mockito.when(onlineBankAccountService.createAccount(any())).thenReturn(account);

        // then
        mvc.perform(MockMvcRequestBuilders.post("/createAccount")
                .content(objectMapper.writeValueAsString(getCreateAccountRequest()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.iban", CoreMatchers.is(account.getIban())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ownerId", CoreMatchers.is(account.getOwnerId())));
    }

    @Test
    public void testCreateOnlineBankingAccountWrongPassFailure() throws Exception {
        // given
        final CreateAccountResponse account = getAccountResponse();
        final CreateAccountRequest createAccountRequest = getCreateAccountRequest();
        createAccountRequest.setPassword("qwe");

        // when
        Mockito.when(onlineBankAccountService.createAccount(any())).thenReturn(account);

        // then
        mvc.perform(MockMvcRequestBuilders.post("/createAccount")
                .content(objectMapper.writeValueAsString(createAccountRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.containsString("Validation failed")));
    }

    @Test
    public void testCreateTokenSuccess() throws Exception {
        // given
        final AuthResponse authResponse = getAuthResponse();

        // when
        Mockito.when(service.checkAndGenerateToken(anyString(), anyString())).thenReturn(authResponse);

        // then
        mvc.perform(MockMvcRequestBuilders.post("/auth")
                .content(objectMapper.writeValueAsString(getAuthRequest()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token", CoreMatchers.is(authResponse.getAccessToken())));
    }

    private CreateAccountResponse getAccountResponse() {
        CreateAccountResponse createAccountResponse = new CreateAccountResponse();
        createAccountResponse.setIban("12345");
        createAccountResponse.setOwnerId("id1235");
        return createAccountResponse;
    }

    private CreateAccountRequest getCreateAccountRequest() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequest();
        createAccountRequest.setAccountNumber("12345");
        createAccountRequest.setPassword("id1235");
        createAccountRequest.setUsername("pokemon");
        return createAccountRequest;
    }

    private AuthResponse getAuthResponse() {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setAccessToken("token12345");
        return authResponse;
    }

    private AuthRequest getAuthRequest() {
        AuthRequest authRequest = new AuthRequest();
        authRequest.setPassword("pass123");
        authRequest.setUsername("username");
        return authRequest;
    }

}
