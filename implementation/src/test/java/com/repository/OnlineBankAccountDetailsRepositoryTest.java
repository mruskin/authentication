package com.repository;

import com.model.OnlineBankAccountDetails;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
public class OnlineBankAccountDetailsRepositoryTest {

    @Autowired
    private OnlineBankAccountDetailsRepository repository;

    @Autowired
    protected TestEntityManager testEntityManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testFindByUsernameSuccess() {
        // given
        OnlineBankAccountDetails bankAccount = getOnlineBankAccountDetails();

        testEntityManager.persist(bankAccount);
        testEntityManager.flush();

        final OnlineBankAccountDetails find = repository.findByUsername(bankAccount.getUsername());

        Assertions.assertThat(find).isNotNull();
    }

    @Test
    public void testNotUniqueUsernameException() {
        // given
        OnlineBankAccountDetails user = getOnlineBankAccountDetails();

        testEntityManager.persist(user);
        testEntityManager.flush();

        // expect
        this.thrown.expect(PersistenceException.class);

        // when
        OnlineBankAccountDetails notUniqueUser = getOnlineBankAccountDetails();
        testEntityManager.persist(notUniqueUser);
        testEntityManager.flush();
    }

    private OnlineBankAccountDetails getOnlineBankAccountDetails() {
        OnlineBankAccountDetails bankAccount = new OnlineBankAccountDetails();
        bankAccount.setId("123");
        bankAccount.setUsername("username");
        bankAccount.setPassword("password");
        bankAccount.setIban("iban1234");
        bankAccount.setAccountNumber("123456");
        return bankAccount;
    }

}
