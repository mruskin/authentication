# Authentication API

RESTful API which authenticates a customer based on his username and password.  
The Authentication API exposed via TLS (8084) using mutual authentication.  

There are 2 endpoints:  
1. one endpoint for creating an online banking account. (A customer can create an online account only if his bank account exists in the Accounts API)  
2. one endpoint for authentication.   

### Run
Run as a standard ```spring-boot``` application.  
To test the app use tests, or POSTMAN.  
Postman json location: ```authentication/implementation/src/test/postman/auth-api.postman_collection.json```